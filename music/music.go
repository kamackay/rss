package music

import (
	"github.com/dhowden/tag"
	"github.com/sirupsen/logrus"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type Parser struct {
	path             *string
	log              *logrus.Logger
	filesMoved       int
	emptyDirsDeleted int
}

func NewParser(path string) *Parser {
	log := logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		ForceColors:   true,
	})
	return &Parser{
		path: &path,
		log:  log,
	}
}

func (parser *Parser) Run() {
	if parser.path == nil {
		return
	}
	parser.log.Info("Processing Music")
	err := filepath.Walk(*parser.path,
		func(path string, info os.FileInfo, err error) error {
			if info == nil || info.IsDir() {
				// Check to see if this is an empty dir, if so, delete
				if info != nil && info.IsDir() {
					err = parser.deleteIfEmpty(path)
					if err != nil {
						parser.log.Warnf("Error deleting empty folder: %+v", err)
					} else {
						parser.emptyDirsDeleted++
					}
				}
				return nil
			}
			if !strings.HasSuffix(path, "mp3") {
				// Not Mp3 File, skip
				return nil
			}
			if err != nil {
				parser.log.Warnf("Error in file: %+v", err)
				return nil
			}
			file, err := os.Open(path)
			if err != nil {
				parser.log.Warnf("Error reading file: %+v", err)
				return nil
			}
			m, err := tag.ReadFrom(file)
			if err != nil {
				//parser.log.Warnf("Error reading tags: %+v", err)
				return nil
			}
			newPath := filepath.Join(*parser.path,
				cleanPath(getArtist(m)),
				cleanPath(m.Album()),
				cleanPath(filepath.Base(path)))
			if newPath != path {
				parser.log.Infof("Moving %s to %s", path, newPath)
				folder := filepath.Dir(newPath)
				err = os.MkdirAll(folder, os.ModePerm)
				if err != nil {
					parser.log.Warnf("Error creating folder: %+v", err)
				}
				if err = os.Rename(path, newPath); err != nil {
					parser.log.Warnf("Couldn't move file! %+v", err)
				} else {
					parser.filesMoved++
				}
			}
			return nil
		})
	if err != nil {
		parser.log.Warnf("Error Iterating files: %+v", err)
	}
	parser.log.Infof("Finished Parsing Music! Moved %d files and Deleted %d empty folders",
		parser.filesMoved, parser.emptyDirsDeleted)
}

func (parser *Parser) deleteIfEmpty(path string) error {
	empty, err := isEmpty(path)
	if err == nil {
		if empty {
			return os.Remove(path)
		}
	} else {
		return err
	}
	return nil
}

func getArtist(meta tag.Metadata) string {
	album := meta.AlbumArtist()
	if len(album) == 0 {
		return meta.Artist()
	}
	return album
}

func isEmpty(name string) (bool, error) {
	f, err := os.Open(name)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1) // Or f.Readdir(1)
	if err == io.EOF {
		return true, nil
	}
	return false, err // Either not empty or error, suits both cases
}

func cleanPath(path string) string {
	regex := regexp.MustCompile("[/\\\\?%*:|\"<>]")
	return strings.TrimSpace(regex.ReplaceAllString(path, ""))
}
