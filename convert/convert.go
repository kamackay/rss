package convert

import (
	"github.com/google/uuid"
	"github.com/kamackay/goffmpeg/models"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/rss/transcoder"
	"time"
)

type StatusEnum int

const (
	InProgress StatusEnum = iota
	Failed
	Done
)

type ProgressFunc = func(progress models.Progress, job Job)

type Converter struct {
	log            *logrus.Logger
	progressUpdate ProgressFunc
	jobs           map[uuid.UUID]*Job
}

type Job struct {
	Id         uuid.UUID  `json:"id"`
	StartTime  int64      `json:"startTime"`
	InputFile  string     `json:"inputFile"`
	Duration   int64      `json:"duration"`
	OutputFile string     `json:"outputFile"`
	Status     StatusEnum `json:"status"`
	Progress   float64    `json:"progress"`
	Error      error      `json:"error"`
	transcoder *transcoder.Transcoder
}

func New(progressUpdate ProgressFunc) *Converter {
	log := logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		ForceColors:   true,
	})
	return &Converter{
		log:            log,
		progressUpdate: progressUpdate,
		jobs:           map[uuid.UUID]*Job{},
	}
}

func (this *Converter) Convert(input string, request Request) uuid.UUID {
	jobId := uuid.New()
	job := &Job{
		Id:         jobId,
		StartTime:  time.Now().UnixNano(),
		Duration:   0,
		InputFile:  input,
		OutputFile: request.OutputFile,
		Status:     InProgress,
	}
	this.jobs[jobId] = job
	go func() {
		trans := new(transcoder.Transcoder)
		job.transcoder = trans

		err := trans.Initialize(input, request.OutputFile, request.Additional)
		if err != nil {
			this.updateJob(jobId, 100, Failed, err)
		}

		trans.MediaFile().SetPreset(request.Preset)
		trans.MediaFile().SetThreads(request.Threads)
		trans.MediaFile().SetCRF(request.CRF)
		//trans.MediaFile().SetBufferSize(200000)

		done := trans.Run(true)

		progress := trans.Output()

		for {
			select {
			case msg := <-progress:
				this.updateJob(jobId, msg.Progress, InProgress, nil)
				this.progressUpdate(msg, this.MustGetJob(jobId))
			case err := <-done:
				if err != nil {
					this.updateJob(jobId, 0, Failed, err)
				} else {
					this.updateJob(jobId, 100, Done, nil)
				}
				return
			}
		}
	}()
	return jobId
}

func (this *Converter) StopJob(id uuid.UUID) error {
	return this.GetJob(id).transcoder.Stop()
}

func (this *Converter) GetJobStr(id string) *Job {
	return this.GetJob(uuid.MustParse(id))
}

func (this *Converter) GetJob(id uuid.UUID) *Job {
	if val, ok := this.jobs[id]; ok {
		return val
	} else {
		return nil
	}
}

func (this *Converter) MustGetJob(id uuid.UUID) Job {
	return *this.GetJob(id)
}

func (this *Converter) updateJob(id uuid.UUID, progress float64, status StatusEnum, err error) {
	job := this.jobs[id]
	job.Duration = time.Now().UnixNano() - job.StartTime
	if err != nil || status == Failed {
		this.log.Warnf("Job %s Has Failed %s", id.String(), err)
		job.Status = Failed
		job.Error = err
	} else {
		job.Status = status
		job.Error = nil
		job.Progress = progress
	}
	this.jobs[id] = job
}

type Request struct {
	OutputFile string   `json:"output"`
	Preset     string   `json:"preset"`
	CRF        uint32   `json:"crf"`
	Additional []string `json:"additional"`
	Threads    int      `json:"threads"`
}
