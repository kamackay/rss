package download

import (
	"io"
	"net/http"
	"os"
)

type Counter struct {
	total   int64
	updates chan int64
}

func (c *Counter) Close() {
	c.updates <- -1
}

func (c *Counter) Write(p []byte) (int, error) {
	n := len(p)
	c.total += int64(n)
	c.updates <- c.total
	return n, nil
}

func New(filename string, url string, updates chan int64) error {
	out, err := os.Create(filename)
	defer out.Close()
	counter := &Counter{updates: updates}
	defer counter.Close()
	if err != nil {
		return err
	}
	resp, err := http.Get(url)
	defer resp.Body.Close()
	if _, err = io.Copy(out, io.TeeReader(resp.Body, counter)); err != nil {
		out.Close()
		return err
	}
	if err != nil {
		return err
	}
	return nil
}
