package model

type Config struct {
	Feeds []Feed `json:"feeds"`
}

type Feed struct {
	Name string `json:"name"`
	Url  string `json:"url"`
	Path string `json:"path"`
}
