package main

import (
	"fmt"
	"github.com/antchfx/xmlquery"
	jsoniter "github.com/json-iterator/go"
	"github.com/kamackay/goffmpeg/models"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/rss/convert"
	"gitlab.com/kamackay/rss/download"
	"gitlab.com/kamackay/rss/model"
	"gitlab.com/kamackay/rss/util"
	"hash/fnv"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"time"
)

const (
	FileRoot        = "/files"
	DownloadThreads = 8
)

type RssFetcher struct {
	log        *logrus.Logger
	dlPool     *util.ThreadPool
	cronEngine *cron.Cron
	pendingDls map[string]interface{}
	mapLock    sync.Mutex
}

func main() {
	log := logrus.New()
	log.SetFormatter(&logrus.TextFormatter{
		FullTimestamp: true,
		ForceColors:   true,
	})
	dlPool := util.NewThreadPool(DownloadThreads, time.Minute)
	cronEngine := cron.New(cron.WithSeconds())
	fetcher := &RssFetcher{
		log:        log,
		dlPool:     &dlPool,
		cronEngine: cronEngine,
		pendingDls: make(map[string]interface{}),
	}
	config, err := readConfig()
	if err != nil {
		log.Infof("Error reading config file: %+v", err)
		return
	}
	dlPool.Start()
	_, err = cronEngine.AddFunc("0 */2 * * * ?", func() {
		config, err := readConfig()
		fetcher.log.Info("Read Config File")
		if err != nil {
			log.Infof("Error reading config file: %+v", err)
			return
		}
		fetcher.fetch(config)
		//music.NewParser("/music").Run()
	})
	if err != nil {
		log.Errorf("Error starting cron: %+v", err)
	}
	cronEngine.Start()
	log.Info("Cron Started")
	fetcher.fetch(config)
	select {}
}

func (this *RssFetcher) fetch(config *model.Config) {
	this.log.Infof("Starting Pull on %d feeds", len(config.Feeds))
	for _, f := range config.Feeds {
		feed := f
		go (func() {
			defer func() {
				if r := recover(); r != nil {
					fmt.Println("Recovered in f", r)
				}
			}()
			dir := filepath.Join(FileRoot, feed.Path)
			this.log.Infof("Beginning to process Feed %s", feed.Name)
			defer this.log.Infof("Done Processing %s", feed.Name)
			resp, err := http.Get(feed.Url)
			if err != nil {
				this.log.Errorf("Error Pulling RSS Feed: %+v", err)
				return
			}
			defer resp.Body.Close()
			doc, err := xmlquery.Parse(resp.Body)
			if err != nil {
				this.log.Errorf("Error Parsing RSS Feed: %+v", err)
				return
			}
			root := xmlquery.FindOne(doc, "//rss")
			if root == nil {
				this.log.Error("Couldn't find root of RSS for %s", feed.Name)
			}
			for _, n := range xmlquery.Find(root, "//channel/item") {
				var title string
				var id string
				var pubDate *time.Time
				if n := n.SelectElement("//title"); n != nil {
					title = n.InnerText()
				}
				if n := n.SelectElement("//pubDate"); n != nil {
					date := n.InnerText()
					// Format: Thu, 09 Sep 2021 10:00:00 -0000
					pubDate, err = tryParse(date)
					if err != nil {
						this.log.Infof("Error Parsing timestamp %s: %+v", date, err)
					}
				}
				if n := n.SelectElement("//guid"); n != nil {
					id = hash(n.InnerText())
				}
				if exists, _, _ := fileInfo(dir); !exists {
					// Folder doesn't exist, create
					if err := os.MkdirAll(dir, 0755); err != nil {
						this.log.Warnf("Couldn't create folder for file %+v", err)
						continue
					}
				}
				if n := n.SelectElement("//enclosure"); n != nil {
					link := n.SelectAttr("url")
					file := filepath.Join(dir, fmt.Sprintf("%s.mp3", strings.TrimSpace(clean(title))))
					if exists, isDir, size := fileInfo(file); exists && !isDir && size > 0 {
						// File exists
						this.log.Debugf("Already have %s (%s) for %s!", id, title, feed.Name)
						continue
					} else {
						this.doDownload(link, file, feed.Name, title, *pubDate)
					}
				} else {
					this.log.Warnf("Couldn't find Link on %s for %s", id, feed.Name)
				}
			}
		})()
	}
}

func tryParse(date string) (t *time.Time, err error) {
	attempts := []func(string) (time.Time, error){
		func(s string) (time.Time, error) {
			return time.Parse("Mon, 02 Jan 2006 15:04:05 -0700", s)
		},
		func(s string) (time.Time, error) {
			return time.Parse("Mon, 02 Jan 2006 15:04:05 MST", s)
		},
	}
	for _, f := range attempts {
		tt, err := f(date)
		t = &tt
		if err == nil {
			return t, err
		}
	}
	return nil, err
}

func (this *RssFetcher) doDownload(link string, filename string, tagName string, episodeName string, date time.Time) {
	this.mapLock.Lock()
	defer this.mapLock.Unlock()
	if _, ok := this.pendingDls[link]; ok {
		// Download is already pending
		return
	}
	this.pendingDls[link] = nil
	updates := make(chan interface{})
	this.dlPool.Add(func() {
		//defer close(updates)
		tf, err := os.CreateTemp("/temp", hash(link))
		tempFile := tf.Name()
		defer func() {
			if err = os.Remove(tempFile); err != nil {
				this.log.Warnf("Couldn't delete temp file! %+v", err)
			}
		}()
		tf.Close()
		if err != nil {
			this.log.Errorf("Couldn't create temp file: %+v", err)
			return
		}
		defer func() {
			this.mapLock.Lock()
			delete(this.pendingDls, link)
			this.mapLock.Unlock()
		}()
		// File doesn't exist. For sure download
		if err := this.download(link, tempFile, updates); err != nil {
			this.log.Errorf("Couldn't Download %+v", err)
		} else {
			c := make(chan interface{}) // Channel to listen to determine when job is done
			//defer close(c)
			this.log.Infof("Moving %s to %s", tempFile, filename)
			var percent float64
			listening := true
			defer func() {
				listening = false
			}()
			converter := convert.New(func(progress models.Progress, job convert.Job) {
				if !listening {
					return
				}
				updates <- nil
				// Keep the job alerted of updates
				if job.Progress > percent+10 {
					this.log.Debugf("Conversion for %s is %f done", filename, job.Progress)
					percent = job.Progress
				}
				if job.Progress >= 100 {
					// Job is done
					this.log.Infof("Job Marked Completed")
					c <- nil
					return
				}
				switch job.Status {
				case convert.Done, convert.Failed:
					this.log.Infof("Job Marked Completed")
					c <- nil
					return
				}
			})
			uuid := converter.Convert(tempFile, convert.Request{
				OutputFile: filename,
				Preset:     "veryslow",
				CRF:        20,
				Threads:    2,
				Additional: []string{
					"-id3v2_version", "3",
					"-metadata", fmt.Sprintf("artist=%s", tagName),
					"-metadata", fmt.Sprintf("artist-sort=%s", tagName),
					"-metadata", fmt.Sprintf("title=%s", episodeName),
					"-metadata", fmt.Sprintf("title-sort=%s", episodeName),
					"-metadata", fmt.Sprintf("album_artist=%s", tagName),
					"-metadata", fmt.Sprintf("album=%s", episodeName),
					"-metadata", fmt.Sprintf("album-sort=%s", episodeName),
					"-metadata", fmt.Sprintf("performer=%s", tagName),
					"-metadata", fmt.Sprintf("date=%s", date.Format("2006-01-02 15:04:05")),
				},
			})
			defer func() {
				time.AfterFunc(time.Minute, func() {
					if err := converter.StopJob(uuid); err != nil {
						this.log.Warnf("Couldn't stop job: %+v", err)
					}
				})
			}()

			// Wait on Conversion to finish
			timeouts := 0
			oldPercent := percent
			for {
				select {
				case <-time.After(time.Second * 10):
					this.log.Infof("Progress: %s at %d%%", filename, int(percent))
					timeouts++
					if oldPercent != percent {
						// there has been a progress update, reset everything
						timeouts = 0
						oldPercent = percent
					} else if timeouts > 6 {
						this.log.Warnf("Job stopping itself: %s", filename)
						return
					}
					if percent >= 90 {
						return
					}
				case <-c:
					this.log.Infof("Job %s has finished", filename)
					return
				}
			}
		}
	}, fmt.Sprintf("Download %s", filename), updates)
}

func (this *RssFetcher) download(link string, filename string, updateChan chan interface{}) error {
	this.log.Infof("Pulling %s to %s", link, filename)
	updates := make(chan int64)
	defer close(updates)
	go func() {
		for {
			select {
			case n := <-updates:
				updateChan <- nil
				if n == -1 {
					this.log.Debugf("Download %s is done", filename)
					return
				}
				this.log.Debugf("Downloaded %d bytes for %s", n, filename)
			case <-time.After(20 * time.Second):
				this.log.Warnf("No Updates for %s in 20 seconds", filename)
			}
		}
	}()
	err := download.New(filename, link, updates)
	if err != nil {
		return err
	}
	this.log.Infof("Downloaded %s to %s", link, filename)
	return nil
}

func clean(filename string) string {
	// Make a Regex to say we only want letters and numbers
	reg := regexp.MustCompile("[^a-zA-Z0-9]+")
	return reg.ReplaceAllString(filename, "")
}

func fileInfo(filename string) (bool, bool, int64) {
	if f, err := os.Stat(filename); os.IsNotExist(err) {
		return false, false, 0
	} else {
		return true, f.IsDir(), f.Size()
	}
}

func hash(s string) string {
	h := fnv.New32a()
	h.Write([]byte(s))
	return fmt.Sprintf("%d", h.Sum32())
}

func readConfig() (*model.Config, error) {
	// Open our jsonFile
	jsonFile, err := os.Open("/config/config.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		return nil, err
	}
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()
	byteValue, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}
	var config model.Config
	err = jsoniter.Unmarshal(byteValue, &config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
