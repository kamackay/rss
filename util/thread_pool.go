package util

import (
	"github.com/dustin/go-humanize"
	"github.com/sirupsen/logrus"
	"gitlab.com/kamackay/dns/logging"
	"strings"
	"sync"
	"time"
)

type ThreadPool struct {
	threadCount uint
	log         *logrus.Logger
	wg          sync.WaitGroup
	jobs        chan *Job
	stopChan    chan interface{}
	start       int64
	allowance   time.Duration
}

type Job struct {
	job     func()
	name    string
	updates chan interface{}
	start   time.Time
	added   time.Time
}

func NewThreadPool(threads uint, allowance time.Duration) ThreadPool {
	return ThreadPool{
		threadCount: threads,
		wg:          sync.WaitGroup{},
		jobs:        make(chan *Job, 1_000_000),
		log:         logging.GetLogger(new(logging.FileHook)),
		allowance:   allowance,
	}
}

func (pool *ThreadPool) Add(job func(), name string, updates chan interface{}) {
	pool.jobs <- &Job{job: job, name: name, updates: updates, added: time.Now()}
	pool.logJobCount()
}

func (pool *ThreadPool) Start() {
	pool.log.Debugf("Starting Thread Pool Size %d with %d jobs",
		pool.threadCount, len(pool.jobs))
	pool.start = time.Now().UnixNano()
	for x := 1; x <= int(pool.threadCount); x++ {
		id := x
		go func() {
			for {
				select {
				case job := <-pool.jobs:
					pool.log.Debugf("Runner %d found job %s, running", id, job.name)
					pool.runJob(job, id)
					pool.logJobCount()
				}
			}
		}()
	}
}

func (pool *ThreadPool) runJob(job *Job, runnerId int) {
	job.start = time.Now()
	returnChan := make(chan interface{}, 1)
	go func() {
		defer func() {
			if r := recover(); r != nil {
				pool.log.Warnf("Recovering from failed Job: %+v", r)
			}
		}()
		job.job()
		returnChan <- nil
	}()
	for {
		select {
		case <-returnChan:
			// Job finished, return
			pool.log.Infof("Job %s has finished (took %s, in queue for %s)", job.name,
				strings.TrimSpace(humanize.RelTime(job.start, time.Now(), "", "")),
				strings.TrimSpace(humanize.RelTime(job.added, job.start, "", "")))
			return
		case <-job.updates:
			// Job has sent a liveness probe, let it be
			break
		case <-time.After(pool.allowance):
			// Job has taken more than a minute, return
			pool.log.Warnf("Job %s on runner #%d has taken more than 1 minute without a probe", job.name, runnerId)
			return
		}
	}
}

func (pool *ThreadPool) logJobCount() {
	pool.log.Infof("Pool now has %d jobs", len(pool.jobs))
}
