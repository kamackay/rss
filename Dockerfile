FROM ubuntu:latest as go
ENV DEBIAN_FRONTEND noninteractive

# Create appuser.
ENV USER=appuser
ENV UID=10001
# See https://stackoverflow.com/a/55757473/12429735RUN
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /app/
WORKDIR $GOPATH/src/gitlab.com/kamackay/rss

RUN apt-get update && apt-get upgrade -y && \
        apt-get install -y software-properties-common g++ && \
        add-apt-repository -y ppa:longsleep/golang-backports && \
        apt-get install -y git apt-utils golang-go ca-certificates && update-ca-certificates

ADD ./go.mod ./

RUN go mod download && \
    go mod verify

COPY ./ ./

RUN GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -tags=jsoniter -o application.file ./index.go \
    && cp ./application.file /app/

FROM ubuntu:latest as final
ENV DEBIAN_FRONTEND noninteractive

# Install Video Conversion Libraries
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y --reinstall ffmpeg apt-utils ca-certificates vim

COPY --from=go /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Import the user and group files from the builder.
COPY --from=go /etc/passwd /etc/passwd
COPY --from=go /etc/group /etc/group
COPY --from=go /app/application.file /server
USER appuser:appuser
WORKDIR /temp
WORKDIR /app

FROM scratch
COPY --from=final / /
ENTRYPOINT ["/server"]
