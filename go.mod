module gitlab.com/kamackay/rss

go 1.16

require (
	github.com/antchfx/xmlquery v1.3.6
	github.com/dhowden/tag v0.0.0-20201120070457-d52dcb253c63
	github.com/djimenez/iconv-go v0.0.0-20160305225143-8960e66bd3da // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/google/uuid v1.3.0
	github.com/json-iterator/go v1.1.10
	github.com/kamackay/goffmpeg v0.5.3
	github.com/mikkyang/id3-go v0.0.0-20191012064224-2c6ab3bb1fbd
	github.com/robfig/cron/v3 v3.0.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/kamackay/dns v0.0.0-20210611212424-e613796aa699
)
